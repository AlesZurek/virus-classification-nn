import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline

# take an array of shape (n, height, width) or (n, height, width, channels)
# and visualize each (height, width) thing in a grid of size approx. sqrt(n) by sqrt(n)
def vis_square(data, padsize=1, padval=0):
    data -= data.min()
    data /= data.max()
    
    # force the number of filters to be square
    n = int(np.ceil(np.sqrt(data.shape[0])))
    padding = ((0, n ** 2 - data.shape[0]), (0, padsize), (0, padsize)) + ((0, 0),) * (data.ndim - 3)
    data = np.pad(data, padding, mode='constant', constant_values=(padval, padval))
    
    # tile the filters into an image
    data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3) + tuple(range(4, data.ndim + 1)))
    data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])
    fig = plt.figure()
    plt.imshow(data)

# Settings
# Make sure that caffe is on the python path:
caffe_root = '/home/hol/caffe/'
models_root = '/home/hol/škola/NSA/virus-classification/data/models/'
data_root = '/home/hol/škola/NSA/virus-classification/data/'

import caffe

plt.rcParams['figure.figsize'] = (10, 10)
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

#----- SETTINGS -----
model_name = 'bvlc_reference_caffenet_edit'
model_name = 'mnist_edit'
#model_name = 'test'
model_name = 'cifar10_41_mirror'
iteration = '101000'
#----- SETTINGS -----

caffe.set_mode_cpu()
net = caffe.Net(models_root + model_name + '/deploy.prototxt',
                models_root + model_name + '/caffenet_train_iter_' + iteration + '.caffemodel',
                caffe.TEST)


# input preprocessing: 'data' is the name of the input blob == net.inputs[0]
transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
transformer.set_transpose('data', (2,0,1))
#transformer.set_mean('data', np.load(data_root + 'imagenet/imagenet_mean.npy').mean(1).mean(1)) # mean pixel
transformer.set_raw_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
#transformer.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB

net.blobs['data'].reshape(1,1,32,32)
net.blobs['data'].data[...] = transformer.preprocess('data', caffe.io.load_image(data_root + 'images_otsu/class-014-sample-083-8bit.png', False))
out = net.forward()

print("Predicted class is #{}.".format(out['prob'].argmax()))

#----- PORN -----
#net = caffe.Net('/www/picturedetector/backend/data/neural-networks/6/deploy.prototxt',
#                '/www/picturedetector/backend/data/neural-networks/6/snapshots/snapshot_iter_330000.caffemodel',
#                caffe.TEST)
#
## input preprocessing: 'data' is the name of the input blob == net.inputs[0]
#transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
#transformer.set_transpose('data', (2,0,1))
#transformer.set_mean('data', np.load('/www/picturedetector/backend/data/neural-networks/6/meanfile.npy').mean(1).mean(1)) # mean pixel
#transformer.set_raw_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
#transformer.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB
#
#net.blobs['data'].reshape(1,3,227,227)
#net.blobs['data'].data[...] = transformer.preprocess('data', caffe.io.load_image('/www/pictures/128/test/rejected/1vkzd93zqv94.jpg'))
#out = net.forward()
#print("Predicted class is #{}.".format(out['prob'].argmax()))
#----- PORN -----
     
               
#[(k, v.data.shape) for k, v in net.blobs.items()]
#[(k, v[0].data.shape) for k, v in net.params.items()]
#
#for k, v in net.params.items():
#    filters = net.params[k][0].data
#    vis_square(filters)
#
#
#       
filters = net.params['conv1'][0].data
vis_square(filters.transpose(0, 2, 3, 1))

feat = net.blobs['conv1'].data[0, :]
vis_square(feat, padval=1)

#filters = net.params['conv2'][0].data
#vis_square(filters)

feat = net.blobs['conv2'].data[0, :36]
vis_square(feat, padval=1)

feat = net.blobs['conv3'].data[0]
vis_square(feat, padval=0.5)

feat = net.blobs['pool3'].data[0]
vis_square(feat, padval=1)

#feat = net.blobs['conv4'].data[0]
#vis_square(feat, padval=0.5)
#
#feat = net.blobs['conv5'].data[0]
#vis_square(feat, padval=0.5)
#
#feat = net.blobs['pool5'].data[0]
#vis_square(feat, padval=1)


#feat = net.blobs['fc6'].data[0]
#plt.subplot(2, 1, 1)
#plt.plot(feat.flat)
#plt.subplot(2, 1, 2)
#_ = plt.hist(feat.flat[feat.flat > 0], bins=100)
#
#feat = net.blobs['fc7'].data[0]
#plt.subplot(2, 1, 1)
#plt.plot(feat.flat)
#plt.subplot(2, 1, 2)
#_ = plt.hist(feat.flat[feat.flat > 0], bins=100)

feat2 = net.blobs['prob'].data[0]
plt.plot(feat2.flat)