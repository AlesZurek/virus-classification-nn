#!/usr/bin/env sh

NOW=$(date +"%d-%m-%Y-%H:%M")
MODEL=$1
ITER=$2
FILE=learning-$MODEL-$NOW.log
echo "LOG ->" $FILE
~/caffe/build/tools/caffe train --solver="/home/hol/škola/NSA/virus-classification/data/models/$MODEL/solver.prototxt" --snapshot=/home/hol/škola/NSA/virus-classification/data/models/$MODEL/caffenet_train_iter_$ITER.solverstate 2> /home/hol/škola/NSA/virus-classification/logs/$FILE
