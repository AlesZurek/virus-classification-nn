#!/bin/bash

MODEL_ROOT='/home/hol/škola/NSA/virus-classification/data/models/'
IMAGES_ROOT='/home/hol/škola/NSA/virus-classification/data/'
IMAGES_FILE='/home/hol/škola/NSA/virus-classification/data/test_images.txt'
SNAPSHOT_PREFIX='caffenet_train_iter_'
SNAPSHOT_POSTFIX='.caffemodel'
START_ITER=20000
MAX_ITER=70000
ITER_STEP=1000

MODEL=$1
IMAGES_FOLDER=$2
COLOR_IMAGES=$3
SHOW_FILES_SCORE=$4

for (( i=$START_ITER; i<=$MAX_ITER; i+=$ITER_STEP ))
do
  python classify_test.py $MODEL_ROOT$MODEL/deploy.prototxt $MODEL_ROOT$MODEL/$SNAPSHOT_PREFIX$i$SNAPSHOT_POSTFIX $IMAGES_FILE $IMAGES_ROOT$IMAGES_FOLDER $COLOR_IMAGES $SHOW_FILES_SCORE 2>/dev/null 
done

