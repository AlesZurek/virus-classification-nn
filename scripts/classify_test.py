# -*- coding: utf-8 -*-
import caffe
import sys
import os
import numpy as np
import matplotlib.pyplot as plt

arguments = sys.argv
argc = len(arguments)

# Default values
images_root = ""
color_images = False
print_files = False

# Inline test - start
model = "/home/hol/škola/NSA/virus-classification/data/models/cifar10_41_mirror/deploy.prototxt"
snapshot = "/home/hol/škola/NSA/virus-classification/data/models/cifar10_41_mirror/caffenet_train_iter_75000.caffemodel"
images_file = "/home/hol/škola/NSA/virus-classification/data/test_images.txt"
images_root = "/home/hol/škola/NSA/virus-classification/data/images_otsu"

model = "/home/hol/škola/NSA/virus-classification/data/models/cifar10_otsu_41/deploy.prototxt"
snapshot = "/home/hol/škola/NSA/virus-classification/data/models/cifar10_otsu_41/backup/29-3-2015/caffenet_train_iter_40000.caffemodel"
# Inline test - end

if argc > 1:
    model = arguments[1]

if argc > 2:
    snapshot = arguments[2]

if argc > 3:
    images_file = arguments[3]

if argc > 4:
    images_root = arguments[4]

if argc > 5:
    color_images = bool(int(arguments[5]))

if argc > 6:
    print_files = bool(int(arguments[6]))

caffe.set_mode_gpu()
net = caffe.Classifier(
    model,
    snapshot,
    #caffe.TEST,
    #mean=mean_data,
    #channel_swap=(2,1,0),
    raw_scale=255
)

images = []
categories = []
images_names = {}
i=0
with open(images_file, "r") as f:
  for line in f:
    values = line.split()
    img_path = os.path.join(images_root, values[0])
    caffe_image = caffe.io.load_image(img_path, color_images)
    images.append(caffe_image)
    categories.append(int(values[1]))
    images_names[i] = values[0]
    i += 1

results = net.predict(images, False)
slice_size = 3
predictions = {}
i = 0
# go through all predicted images
for scores in results:
    # get category id with best match
    categoryIds = (-scores).argsort()
    
    if slice_size != 0:
        categoryIds = categoryIds[:slice_size]
    
    # save prediction results
    pred_categ = []
    for id in categoryIds:
        pred_categ.append({"category":id,"percentage":float(scores[id])})
    
    predictions[i] = pred_categ;
    i += 1

print_results = []
start = 0
correct = 0
wrong = 0
wrong_false = 0
wrong_true = 0
wrong_60 = 0
wrong_70 = 0
wrong_80 = 0
wrong_90 = 0
wrong_100 = 0
correct_sum = 0
wrong_sum = 0
wrong_false_sum = 0
wrong_true_sum = 0
correct_files = []
wrong_files = []
wrong_true_files = []
failed_classify = 0

confusion_matrix = {}
for i in range(0,15):
    confusion_matrix[i] = {}
    for j in range(0,15):
        confusion_matrix[i][j] = 0

for i in predictions:
    correct_category = categories[i]
    # Kategorie v DB zacina od 1, v caffe zacina od 0
    category = (predictions[i][0]['category'])
    percentage = predictions[i][0]['percentage']
    categoryPrint = category + 1
    confusion_matrix[correct_category][category] += 1
    
    if category == correct_category:
        correct += 1
        correct_sum += percentage
        message='OK'
        correct_files.append("{0:.4f}".format(percentage)  + " -> " + str(categoryPrint) + " \t" + images_names[i])
    else:
        wrong += 1
        wrong_sum += percentage
        message='Wrong'
        wrong_false += 1
        wrong_false_sum += percentage
        other_categories = []
        for prediction in predictions[i]:
            other_categories.append(str(prediction['category'] + 1) + ' -> ' + "{0:.4f}".format(prediction['percentage']))
        
        wrong_files.append("{0:.4f}".format(percentage)  + " -> " + str(categoryPrint) + " \t" + images_names[i] + "\t[" + " | ".join(other_categories) + "]")

print"----------------------------------------"
print snapshot

if print_files:
    print"----------------------------------------"
    print"Correct files"
    
    for line in correct_files:
        print line
    
    print"----------------------------------------"
    print"Wrong files"
    
    for line in wrong_files:
        print line

print"----------------------------------------"
print"Confusion Matrix"
line = '   |'
for i in range(0,15):
    line += '  ' + "{:2d}".format(i)
print '+----+-------------------------------------------------------------+'
print '| ' + line + ' |'
print '+----+-------------------------------------------------------------+'
for row in confusion_matrix:
    line = "{:2d}".format(row) + ' |'
    for column in confusion_matrix[row]:
        line += "  " + "{:2d}".format(confusion_matrix[row][column])
    print '| ' + line + ' |'
print '+----+-------------------------------------------------------------+'
print"----------------------------------------"
print"Average values"

print "Correct:\t" + "{0:.4f}".format(correct_sum/correct)
print "Wrong:\t\t" + "{0:.4f}".format(wrong_sum/wrong)

print"----------------------------------------"
print"Final results"
print"Accuracy:\t" + "{0:.2f}".format(float(correct*100)/(correct + wrong)) + '%'
print"Correct:\t" + str(correct)
print"Wrong:\t\t" + str(wrong)
print"Total:\t\t" + str(correct + wrong)
    

#import numpy as np
#import matplotlib.pyplot as plt
#
#conf_arr = [[33,2,0,0,0,0,0,0,0,1,3], 
#            [3,31,0,0,0,0,0,0,0,0,0], 
#            [0,4,41,0,0,0,0,0,0,0,1], 
#            [0,1,0,30,0,6,0,0,0,0,1], 
#            [0,0,0,0,38,10,0,0,0,0,0], 
#            [0,0,0,3,1,39,0,0,0,0,4], 
#            [0,2,2,0,4,1,31,0,0,0,2],
#            [0,1,0,0,0,0,0,36,0,2,0], 
#            [0,0,0,0,0,0,1,5,37,5,1], 
#            [3,0,0,0,0,0,0,0,0,39,0], 
#            [0,0,0,0,0,0,0,0,0,0,38]]
#
#conf_arr = confusion_matrix
#norm_conf = []
#for i in conf_arr:
#    a = 0
#    tmp_arr = []
#    a = sum(i, 0)
#    for j in i:
#        tmp_arr.append(float(j)/float(a))
#    norm_conf.append(tmp_arr)
#
#fig = plt.figure()
#plt.clf()
#ax = fig.add_subplot(111)
#ax.set_aspect(1)
#res = ax.imshow(np.array(norm_conf), cmap=plt.cm.jet, 
#                interpolation='nearest')
#
#width = len(conf_arr)
#height = len(conf_arr[0])
#
#for x in xrange(width):
#    for y in xrange(height):
#        ax.annotate(str(conf_arr[x][y]), xy=(y, x), 
#                    horizontalalignment='center',
#                    verticalalignment='center')
#
#cb = fig.colorbar(res)
#alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
#plt.xticks(range(width), alphabet[:width])
#plt.yticks(range(height), alphabet[:height])
#plt.savefig('confusion_matrix.png', format='png')
#plt.show()
    