#!/usr/bin/env python
# -*- coding: utf-8 -*-

import caffe
import numpy

dataFolder = "/home/hol/škola/NSA/virus-classification/data/"
imagenetFolder = dataFolder + "imagenet_bw/"
imageFolder = dataFolder + "images_bw/"
modelsFolder = dataFolder + "models/"

image_path = imageFolder + "/class-010-sample-043-8bit.png"
model_config = modelsFolder + "mnist_edit/deploy.prototxt"
snapshot_path = modelsFolder + "mnist_edit/caffenet_train_iter_10000.caffemodel"
mean_file_path = imagenetFolder + "imagenet_mean.npy"
mean_data = numpy.load(mean_file_path).mean(1).mean(1)
caffe.set_mode_gpu()
net = caffe.Classifier(
    model_config,
    snapshot_path,
    caffe.TEST,
    #mean=mean_data,
    #channel_swap=(2,1,0),
    #raw_scale=255
)

input_image = caffe.io.load_image(image_path, False)
input_images=[]
input_images.append(input_image)

# input preprocessing: 'data' is the name of the input blob == net.inputs[0]
transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
transformer.set_transpose('data', (2,0,1))
transformer.set_mean('data', mean_data) # mean pixel
#transformer.set_raw_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
#transformer.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB

net.blobs['data'].reshape(1,3,41,41)
net.blobs['data'].data[...] = transformer.preprocess('data', caffe.io.load_image(image_path))
prediction = net.forward()
print("Predicted class is #{}.".format(prediction['prob'].argmax()))

#print input_images

#prediction = net.predict(input_images)
print 'prediction shape:', prediction['prob'].shape
#plt.plot(prediction[0])
print 'predicted class:', prediction['prob'].argmax()