#!/bin/sh

#variables
DELIMETER=';'
HEADER="iteration"$DELIMETER"accuracy"$DELIMETER"loss"

#temporary file names
TMP_FILE1="tmp_file_121851_csv"
TMP_FILE2="tmp_file_121852_csv"
TMP_FILE3="tmp_file_121853_csv"

#iteration
grep -oP '(?<=Iteration[[:space:]])[[:digit:]]*(?=,[[:space:]]Testing[[:space:]]net)' $1 > $TMP_FILE1

#accuracy
grep -oP '(?<=accuracy[[:space:]]=[[:space:]])[[:digit:]]*.[[:digit:]]*' $1 > $TMP_FILE2

#loss
grep -oP '(?<=#1\:[[:space:]]loss[[:space:]]=[[:space:]])[[:digit:]]*.[[:digit:]]*' $1 > $TMP_FILE3

#create header
echo $HEADER > "$1_stats.csv"

#join file
paste -d $DELIMETER $TMP_FILE1 $TMP_FILE2 $TMP_FILE3 >> "$1_stats.csv"

#remove temporary files
rm $TMP_FILE1 $TMP_FILE2 $TMP_FILE3

