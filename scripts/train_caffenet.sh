#!/usr/bin/env sh

CAFFE_FOLDER="/home/hol/caffe"
PROJECT_FOLDER="/home/hol/škola/NSA/virus-classification"

NOW=$(date +"%d-%m-%Y-%H:%M")
MODEL=$1
MODEL_FOLDER="$PROJECT_FOLDER/data/models/$MODEL"
FILE=learning-$MODEL-$NOW.log
PWD=$(pwd)
cd $MODEL_FOLDER
echo "LOG ->" $FILE
$CAFFE_FOLDER/build/tools/caffe train --solver="$MODEL_FOLDER/solver.prototxt" 2> "$PROJECT_FOLDER/logs/$FILE"
cd $PWD
